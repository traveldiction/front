import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { AuthService } from 'angularx-social-login';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})

export class ProfileComponent implements OnInit {
  public user =  new User();

  constructor(private authService: AuthService, private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  goToHome() {
    this.userService.prediction(this.user).subscribe((res: any) => {
      this.user.prediction = res.country;
      localStorage.setItem('user', JSON.stringify(this.user));
      this.router.navigate(['/home']);
    });
  }
}
