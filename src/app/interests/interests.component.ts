import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/user.model';

@Component({
  selector: 'app-interests',
  templateUrl: './interests.component.html',
  styleUrls: ['./interests.component.scss']
})

export class InterestsComponent implements OnInit {
  public user =  new User();

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.user.socialUser = JSON.parse(localStorage.getItem('usersocial'));
    this.user.interests = [];
  }

  ajouterInterests(interest) {
    if (this.user.interests.length < 3 && !this.user.interests.includes(interest)) {
      this.user.interests.push(interest);
    }
  }

  redirectContinuer() {
    localStorage.setItem('user', JSON.stringify(this.user));
    this.router.navigate(['/form']);
  }
}
