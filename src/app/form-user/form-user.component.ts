import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LanguageModel } from 'src/app/models/language.model';
import { StatusModel } from 'src/app/models/status.model';
import { ReligionModel } from 'src/app/models/religion.model';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { Country } from '@angular-material-extensions/select-country';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.scss']
})
export class FormUserComponent implements OnInit {
  public user =  new User();
  public languageModel = LanguageModel;
  public statusModel = StatusModel;
  public religionModel = ReligionModel;
  submitted = false;
  loginForm: FormGroup;

  constructor(private fb: FormBuilder, private router: Router, private userService: UserService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.loginForm = this.fb.group({
      gender: ['', [Validators.required]],
      age: ['', [Validators.required]],
      language : ['', [Validators.required]],
      status : ['', [Validators.required]],
      religion: ['', [Validators.required]]
    });
  }

  changeSuit(name, e) {
    this.loginForm.get(name).setValue(e.target.value, {
      onlySelf: true
    });
  }

  onCountrySelected(event: Country) {
    this.user.country = event.name;
  }

  public handleError = (controlName: string, errorName: string) => {
    return this.loginForm.controls[controlName].hasError(errorName);
  }

  login() {
    if (this.loginForm.valid) {
      this.submitted = true;
      this.user.gender = this.loginForm.get('gender').value;
      this.user.age = this.loginForm.get('age').value;
      this.user.religion = this.loginForm.get('religion').value;
      this.user.status = this.loginForm.get('status').value;
      this.user.language = this.loginForm.get('language').value;
      localStorage.setItem('user', JSON.stringify(this.user));
      this.router.navigate(['/profile']);
    }
  }

  fileValsLanguage(): Array<string> {
    const keys = Object.keys(this.languageModel);
    return keys.map(el => Object(this.languageModel)[el]);
  }

  fileValsStatus(): Array<string> {
    const keys = Object.keys(this.statusModel);
    return keys.map(el => Object(this.statusModel)[el]);
  }

  fileValsReligion(): Array<string> {
    const keys = Object.keys(this.religionModel);
    return keys.map(el => Object(this.religionModel)[el]);
  }
}
