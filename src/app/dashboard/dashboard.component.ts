import { Component, OnInit } from '@angular/core';
import { AuthService } from 'angularx-social-login';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public user =  new User();

  constructor(private authService: AuthService, private router: Router, private userService: UserService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  backHome() {
    this.router.navigate(['/home']);
  }

  subscribeTable() {
    this.userService.register(this.user).subscribe((res: any) => {
      if (res.socialUser.email === this.user.socialUser.email &&
        res.socialUser.id === this.user.socialUser.id) {
        setTimeout(() => this.signOut(), 3000);
      } else {
        console.error('error caught');
      }
    });
  }

  signOut(): void {
    this.authService.signOut().then(r => {
        this.router.navigate(['']);
      }
    );
  }
}
