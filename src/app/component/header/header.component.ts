import { Component, OnInit } from '@angular/core';
import {AuthService} from 'angularx-social-login';
import {Router} from '@angular/router';
import {User} from '../../models/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public user =  new User();

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void { }

  accessProfile() {
    this.router.navigate(['/profile']);
  }

  signOut(): void {
    this.authService.signOut().then(r => {
      this.router.navigate(['']);
      localStorage.removeItem('user');
      }
    );
  }

}
