import { SocialUser } from '../../../node_modules/angularx-social-login/lib/entities/user';
import { InterestsModel } from './interests.model';
import { ReligionModel } from './religion.model';
import { StatusModel } from './status.model';
import { LanguageModel } from './language.model';

export class User {
  id: string;
  socialUser: SocialUser;
  interests: Array<InterestsModel>[];
  age: number;
  gender: string;
  country: string;
  religion: ReligionModel;
  status: StatusModel;
  language: LanguageModel;
  prediction: Array<string>[];
}


