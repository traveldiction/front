export enum StatusModel {
  ENTRY1 = 'Single',
  ENTRY2 = 'Married',
  ENTRY3 = 'Divorced',
  ENTRY4 = 'Attached',
  ENTRY5 = 'Widow/Widower',
  ENTRY6 = 'Separated'
}
