export enum InterestsModel {
  theatre,
  sports,
  books,
  museums,
  art,
  concerts,
  movies,
  photography,
  painting,
  food
}
