export enum LanguageModel {
  ENTRY1 = 'Tagalog', ENTRY2 = 'English', ENTRY3 = 'Spanish', ENTRY4 = 'French', ENTRY5 = 'Dutch', ENTRY6 = 'Italian',
  ENTRY7 = 'Norwegian', ENTRY8 = 'Telugu', ENTRY9 = 'Malay', ENTRY10 = 'Bulgarian', ENTRY11 = 'Estonian',
  ENTRY12 = 'Hungarian', ENTRY13 = 'Persian', ENTRY14 = 'German', ENTRY15 = 'Finnish', ENTRY16 = 'Danish', ENTRY17 = 'Slovenian',
  ENTRY18 = 'Turkish', ENTRY19 = 'Afrikaans', ENTRY20 = 'Russian', ENTRY22 = 'Portuguese', ENTRY23 = 'Lithuanian',
  ENTRY24 = 'Marathi', ENTRY25 = 'Japanese', ENTRY26 = 'Polish', ENTRY27 = 'Indian', ENTRY28 = 'Mandarin', ENTRY29 = 'Romanian',
  ENTRY30 = 'Cantonese', ENTRY31 = 'Greek', ENTRY32 = 'Burmese', ENTRY33 = 'Arabic', ENTRY34 = 'Indonesian', ENTRY35 = 'Latvian',
  ENTRY36 = 'Swedish', ENTRY37 = 'Belgian', ENTRY38 = 'Bengali', ENTRY39 = 'Hindi', ENTRY40 = 'Urdu', ENTRY41 = 'Sign',
  ENTRY42 = 'Icelandic', ENTRY43 = 'Tamil', ENTRY44 = 'Vietnamese', ENTRY45 = 'Ukrainian', ENTRY46 = 'Esperanto',
  ENTRY47 = 'Taiwanese', ENTRY48 = 'Thai', ENTRY49 = 'Serbian', ENTRY50 = 'Korean', ENTRY51 = 'Croatian', ENTRY52 = 'Hebrew',
  ENTRY53 = 'Visayan', ENTRY54 = 'Nepalese', ENTRY55 = 'Punjabi', ENTRY56 = 'Gujrati', ENTRY57 = 'Welsh', ENTRY58 = 'Moldovian',
  ENTRY59 = 'Czech'
}
