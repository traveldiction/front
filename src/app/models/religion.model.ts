export enum ReligionModel {
  ENTRY1 = 'Other Religion',
  ENTRY2 = 'Christianity',
  ENTRY3 = 'Islam',
  ENTRY4 = 'Buddhism',
  ENTRY5 = 'Agnostic',
  ENTRY6 = 'Atheist',
  ENTRY7 = 'Hinduism',
  ENTRY8 = 'Judaism',
  ENTRY9 = 'Adventist',
  ENTRY10 = 'Scientology',
  ENTRY11 = 'Not Telling'
}
