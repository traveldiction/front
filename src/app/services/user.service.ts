import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  })
};

@Injectable({ providedIn: 'root' })
export class UserService {
  baseUrl = 'http://localhost:8080/user/';

  constructor(private http: HttpClient) {}

  register(model: any): Observable<User> {
    let response;
    response =  this.http.post(this.baseUrl + 'registration', model, httpOptions);
    return response;
  }

  prediction(model: any) {
    return this.http.post<User>(this.baseUrl + 'predict', model, httpOptions);
  }

}
